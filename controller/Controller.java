package controller;

import model.Simulator;
import view.View;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller {
    private View view;
    private Simulator simulator;
    private Thread threadSimulare;

    public Controller(View view) {
        this.view = view;
        view.addTimpSimulareListener(new TimpSimulareListener());
        view.addTimpMinProcListener(new TimpMinProcListener());
        view.addTimpMaxProcListener(new TimpMaxProcListener());
        view.addNumarCaseListener(new NumarCaseListener());
        view.addNumarClientiListener(new NumarClientiListener());
        view.addSpeedListener(new SpeedListener());
        view.addPlayListener(new PlayListener());
    }

    class TimpSimulareListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String text = view.getTfTimpSimulare().getText();
            if ((!text.matches("[0-9]+")) || (((Integer.parseInt(text) < 60) || (Integer.parseInt(text) > 10000)))) {
                JOptionPane.showMessageDialog(view.getFrame(), "Reintroduceti valoarea. Timpul de simulare trebuie sa fie in intervalul [60, 10000].", "Valoare incorecta.", JOptionPane.WARNING_MESSAGE);
            }
        }
    }

    class TimpMinProcListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String text = view.getTfTimpMinimDeProcesare().getText();
            if (!text.matches("[0-9]+") || (Integer.parseInt(text) < 1 || Integer.parseInt(text) > Integer.parseInt(view.getTfTimpSimulare().getText()) / 10)) {
                JOptionPane.showMessageDialog(view.getFrame(), "Reintroduceti valoarea. Timpul minim de procesare trebuie sa fie intre 1 si " + Integer.parseInt(view.getTfTimpSimulare().getText()) / 10, "Valoare incorecta.", JOptionPane.WARNING_MESSAGE);
            }
        }
    }

    class TimpMaxProcListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String text = view.getTfTimpMaximDeProcesare().getText();
            if (!text.matches("[0-9]+") || (Integer.parseInt(text) < 1 || Integer.parseInt(text) < Integer.parseInt(view.getTfTimpMinimDeProcesare().getText()) || Integer.parseInt(text) > Integer.parseInt(view.getTfTimpSimulare().getText()) / 5)) {
                JOptionPane.showMessageDialog(view.getFrame(), "Reintroduceti valoarea. Timpul minim de procesare trebuie sa fie intre " + view.getTfTimpMinimDeProcesare().getText() + " si " + Integer.parseInt(view.getTfTimpSimulare().getText()) / 5, "Valoare incorecta.", JOptionPane.WARNING_MESSAGE);
            }
        }
    }

    class NumarCaseListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String text = view.getTfNumarulDeCase().getText();
            if (!text.matches("[0-9]+") || (Integer.parseInt(text) < 1 || Integer.parseInt(text) > 100)) {
                JOptionPane.showMessageDialog(view.getFrame(), "Reintroduceti valoarea. Maxim 100 de case.", "Valoare incorecta.", JOptionPane.WARNING_MESSAGE);
            }
        }
    }

    class NumarClientiListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String text = view.getTfNumarulDeClienti().getText();
            if (!text.matches("[0-9]+") || (Integer.parseInt(text) < 1 || Integer.parseInt(text) > 100)) {
                JOptionPane.showMessageDialog(view.getFrame(), "Reintroduceti valoarea. Maxim 100 de clienti.", "Valoare incorecta.", JOptionPane.WARNING_MESSAGE);
            }
        }
    }

    class SpeedListener implements ChangeListener {
        @Override
        public void stateChanged(ChangeEvent e) {
            if (!view.getSpeedSlider().getValueIsAdjusting())
                Simulator.setAccelerare((int) view.getSpeedSlider().getValue());
        }
    }

    class PlayListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (view.getTfTimpSimulare().getText().isEmpty() || view.getTfTimpMinimDeProcesare().getText().isEmpty() || view.getTfTimpMaximDeProcesare().getText().isEmpty()
                    || view.getTfNumarulDeCase().getText().isEmpty() || view.getTfNumarulDeClienti().getText().isEmpty()) {
                JOptionPane.showMessageDialog(view.getFrame(), "Introduceti toate datele necesare simularii.", "Date incomplete.", JOptionPane.WARNING_MESSAGE);
                view.getTfTimpSimulare().setText("");
                view.getTfTimpMinimDeProcesare().setText("");
                view.getTfTimpMaximDeProcesare().setText("");
                view.getTfNumarulDeCase().setText("");
                view.getTfNumarulDeClienti().setText("");
            } else {
                View.liveInfo.setText("");
                View.celMaiAglomeratMoment.setText("            ");
                View.timpMediuAsteptare.setText("   ");
                simulator = new Simulator(Integer.parseInt(view.getTfTimpSimulare().getText()), Integer.parseInt(view.getTfTimpMinimDeProcesare().getText()),
                        Integer.parseInt(view.getTfTimpMaximDeProcesare().getText()), Integer.parseInt(view.getTfNumarulDeCase().getText()),
                        Integer.parseInt(view.getTfNumarulDeClienti().getText()));
                threadSimulare = new Thread(simulator);
                threadSimulare.run();
            }
        }
    }

    public void showTheFrame() {
        view.getFrame().setVisible(true);
    }
}
