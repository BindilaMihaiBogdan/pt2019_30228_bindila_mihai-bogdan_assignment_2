package model;

public class Client implements Comparable<Client> {
    private int numarClient;
    private int timpulSosirii;
    private int timpulDeAsteptare;
    private int timpulDeProcesare;
    private int timpulPlecari;

    public Client(int numarClient, int timpulSosirii, int timpulDeProcesare) {
        this.numarClient = numarClient;
        this.timpulSosirii = timpulSosirii;
        this.timpulDeAsteptare = 0;
        this.timpulDeProcesare = timpulDeProcesare;
        this.timpulPlecari = 0;
    }

    @Override
    public int compareTo(Client o) {
        if (this.getTimpulSosirii() < o.getTimpulSosirii())
            return -1;
        else if (this.getTimpulSosirii() > o.getTimpulSosirii())
            return 1;
        else return 0;
    }

    public int getNumarClient() {
        return numarClient;
    }

    public int getTimpulSosirii() {
        return timpulSosirii;
    }

    public int getTimpulDeAsteptare() {
        return timpulDeAsteptare;
    }

    public int getTimpulDeProcesare() {
        return timpulDeProcesare;
    }

    public int getTimpulPlecari() {

        return timpulPlecari;
    }

    public void setNumarClient(int numarClient) {
        this.numarClient = numarClient;
    }

    public void setTimpulDeAsteptare(int timpulDeAsteptare) {
        this.timpulDeAsteptare = timpulDeAsteptare;
    }

    public void setTimpulPlecari(int timpulPlecari) {
        this.timpulPlecari = timpulPlecari;
    }
}