package model;

import view.View;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class CasaDeMarcat implements Runnable {
    private AtomicInteger timpDeAsteptareCurent;
    private BlockingQueue<Client> clienti;
    private String numeCasa;
    private boolean primulLaCasa;
    private int timpDeAsteptareAlPrecedentului;
    private int timpulDeSosireAlPrecedentului;
    private int timpDeAsteptareTotalLaCasa;

    public CasaDeMarcat(String numeCasa) {
        this.timpDeAsteptareCurent = new AtomicInteger(0);
        this.clienti = new LinkedBlockingQueue<>();
        this.numeCasa = numeCasa;
        this.primulLaCasa = true;
        this.timpDeAsteptareAlPrecedentului = 0;
        this.timpulDeSosireAlPrecedentului = 0;
        this.timpDeAsteptareTotalLaCasa = 0;
    }

    public synchronized void adaugaClient(Client client) {
        int aux = 0;
        try {
            if (primulLaCasa) {
                timpDeAsteptareCurent.set(client.getTimpulDeProcesare());
                client.setTimpulDeAsteptare(0);
                View.liveInfo.setText(View.liveInfo.getText() + "Primul la " + this.getNumeCasa() + "\n");
            } else {
                aux = timpDeAsteptareAlPrecedentului - (client.getTimpulSosirii() - timpulDeSosireAlPrecedentului);
                if (aux < 0) {
                    timpDeAsteptareCurent.set(client.getTimpulDeProcesare());
                    client.setTimpulDeAsteptare(0);
                } else {
                    timpDeAsteptareCurent.set(aux + client.getTimpulDeProcesare());
                    client.setTimpulDeAsteptare(aux);
                    timpDeAsteptareTotalLaCasa += client.getTimpulDeAsteptare();
                }
            }
            timpulDeSosireAlPrecedentului = client.getTimpulSosirii();
            if (!primulLaCasa)
                timpDeAsteptareAlPrecedentului = timpDeAsteptareCurent.getAndAdd(0);
            else
                timpDeAsteptareAlPrecedentului = timpDeAsteptareCurent.getAndAdd(0);
            if (primulLaCasa) {
                primulLaCasa = false;
                client.setTimpulPlecari(client.getTimpulDeProcesare() + client.getTimpulSosirii());
            } else
                client.setTimpulPlecari(timpDeAsteptareAlPrecedentului + client.getTimpulSosirii());
            clienti.put(client);
            View.liveInfo.setText(View.liveInfo.getText() + Simulator.getTimpCurent() + ": Clientul " + client.getNumarClient() + " adaugat la " + this.getNumeCasa() + ": TA " + timpDeAsteptareCurent + " TP " + client.getTimpulPlecari() + "\n");
        } catch (InterruptedException e) {
            View.liveInfo.setText(View.liveInfo.getText() + "Clientul nu s-a putut pune la coada.\n");
        }
        notify();
    }

    @Override
    public void run() {
        int numarClient = -1;
        int timpDeScoatere = -1;
        while (true) {
            try {
                numarClient = -1;
                timpDeScoatere = -1;
                for (Client client : clienti) {
                    numarClient = client.getNumarClient();
                    timpDeScoatere = client.getTimpulPlecari();
                    break;
                }
                for (int i = 1; i <= 60; i++)
                    if (i == timpDeScoatere && numarClient != -1 && timpDeScoatere == Simulator.getTimpCurent()) {
                        clienti.take();
                        View.liveInfo.setText(View.liveInfo.getText() + Simulator.getTimpCurent() + ": Clientul " + numarClient + " de la " + this.getNumeCasa() + " a fost servit.\n");
                    }
            } catch (InterruptedException e) {
                View.liveInfo.setText(View.liveInfo.getText() + "Clientul nu a putut iesii din coada.\n");
            }
            if (Simulator.getTimpCurent() == Simulator.getTimpSimulare()) {
                Thread.currentThread().interrupt();
            }
        }
    }

    public AtomicInteger getTimpDeAsteptareCurent() {
        return timpDeAsteptareCurent;
    }


    public String getNumeCasa() {
        return numeCasa;
    }


    public int getTimpDeAsteptareTotalLaCasa() {
        return timpDeAsteptareTotalLaCasa;
    }

    public BlockingQueue<Client> getClienti() {
        return clienti;
    }

    public String afiseazaCoadaDeLaCasaCurenta() {
        String coadaCurenta = new String();
        if (this.getNumeCasa().equals("casa 1"))
            coadaCurenta += "=================================================================================================\n";
        coadaCurenta += this.getNumeCasa().toUpperCase() + " <= ";
        for (Client client : clienti)
            coadaCurenta += client.getNumarClient() + " ";
        coadaCurenta += "\n";
        coadaCurenta += "=================================================================================================";
        coadaCurenta += "\n";
        return coadaCurenta;
    }
}