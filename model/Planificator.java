package model;

import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicInteger;

public class Planificator {
    private LinkedList<CasaDeMarcat> caseDeMarcat;
    private int numarulMaximDeCaseDeMarcat;

    public Planificator(int numarulMaximDeCaseDeMarcat) {
        this.caseDeMarcat = new LinkedList<>();
        this.numarulMaximDeCaseDeMarcat = numarulMaximDeCaseDeMarcat;
        for (int i = 0; i < this.numarulMaximDeCaseDeMarcat; i++) {
            CasaDeMarcat casaDeMarcat = new CasaDeMarcat("casa " + (i + 1));
            caseDeMarcat.add(casaDeMarcat);
        }
    }

    public int adaugaUnClientLaCoada(Client client) {
        AtomicInteger timpMinimDeAsteptare = new AtomicInteger(Integer.MAX_VALUE);
        int index = -1;
        int contor = 0;
        for (CasaDeMarcat casaDeMarcat : this.caseDeMarcat) {
            if (casaDeMarcat.getTimpDeAsteptareCurent().get() < timpMinimDeAsteptare.get()) {
                timpMinimDeAsteptare = casaDeMarcat.getTimpDeAsteptareCurent();
                index = contor;
            }
            contor++;
        }
        caseDeMarcat.get(index).adaugaClient(client);
        return index;
    }

    public LinkedList<CasaDeMarcat> getCaseDeMarcat() {
        return caseDeMarcat;
    }

    public int getNumarulMaximDeCaseDeMarcat() {
        return numarulMaximDeCaseDeMarcat;
    }
}