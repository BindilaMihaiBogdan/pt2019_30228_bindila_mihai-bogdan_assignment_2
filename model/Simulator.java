package model;

import view.View;

import java.util.Collections;
import java.util.LinkedList;
import java.util.Random;

public class Simulator implements Runnable {
    private static int accelerare = 1;
    private static int timpCurent;
    private static int timpSimulare;
    private int timpulMinimDeProcesare;
    private int timpulMaximDeProcesare;
    private int numarulDeClienti;
    private LinkedList<Thread> threadsCaseDeMarcat;
    private Planificator planificator;
    private LinkedList<Client> clientiiMagazinului;
    private String toateCozile;
    private float timpMediuDeAsteptare;
    private int numarulMaximDeOameniInMagazin;
    private int celMaiAglomeratMoment;

    public Simulator(int timpDeSimulare, int timpulMinimDeProcesare, int timpulMaximDeProcesare, int numarulDeCase, int numarulDeClienti) {
        timpCurent = 0;
        timpSimulare = timpDeSimulare;
        this.timpulMinimDeProcesare = timpulMinimDeProcesare;
        this.timpulMaximDeProcesare = timpulMaximDeProcesare;
        this.numarulDeClienti = numarulDeClienti;
        this.planificator = new Planificator(numarulDeCase);
        this.clientiiMagazinului = new LinkedList<>();
        threadsCaseDeMarcat = new LinkedList<>();
        for (int i = 0; i < planificator.getNumarulMaximDeCaseDeMarcat(); i++) {
            Thread thread = new Thread(planificator.getCaseDeMarcat().get(i), planificator.getCaseDeMarcat().get(i).getNumeCasa());
            threadsCaseDeMarcat.add(thread);
            thread.start();
        }
        genereazaClientii(this.numarulDeClienti);
        this.timpMediuDeAsteptare = 0;
        this.numarulMaximDeOameniInMagazin = 0;
        this.celMaiAglomeratMoment = -1;
    }

    public void genereazaClientii(int numarulDeClienti) {
        int timpulSosirii;
        int timpDeProcesare;
        Random rand;
        for (int i = 0; i < numarulDeClienti; i++) {
            rand = new Random();
            timpulSosirii = rand.nextInt(timpSimulare - timpulMaximDeProcesare);
            rand = new Random();
            timpDeProcesare = rand.nextInt(timpulMaximDeProcesare - timpulMinimDeProcesare);
            timpDeProcesare += timpulMinimDeProcesare;
            clientiiMagazinului.add(new Client(0, timpulSosirii, timpDeProcesare));
        }
        Collections.sort(clientiiMagazinului);
        View.liveInfo.setText(View.liveInfo.getText() + "  Timp de sosire  |  Timp de procesare\n");
        int numarClient = 1;
        for (Client client : this.clientiiMagazinului) {
            View.liveInfo.setText(View.liveInfo.getText() + String.format("%17d", client.getTimpulSosirii()) + "                " + String.format("%17d", client.getTimpulDeProcesare()) + "\n");
            client.setNumarClient(numarClient);
            numarClient++;
        }
    }

    @Override
    public synchronized void run() {
        int aux;
        while (timpCurent < timpSimulare) {
            for (Client client : this.clientiiMagazinului) {
                if (client.getTimpulSosirii() == timpCurent) {
                    notifyAll();
                    planificator.adaugaUnClientLaCoada(client);
                }
            }
            timpCurent++;
            {
                try {
                    Thread.sleep(1000 / accelerare);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            toateCozile = new String();
            aux = 0;
            for (CasaDeMarcat casaDeMarcat : planificator.getCaseDeMarcat()) {
                toateCozile = toateCozile + casaDeMarcat.afiseazaCoadaDeLaCasaCurenta();
                aux += casaDeMarcat.getClienti().size();
            }
            if (aux > numarulMaximDeOameniInMagazin) {
                numarulMaximDeOameniInMagazin = aux;
                celMaiAglomeratMoment = timpCurent;
            }
            View.caseDeMarcat.setText(toateCozile);
            if (timpCurent == timpSimulare) {
                for (CasaDeMarcat casaDeMarcat : planificator.getCaseDeMarcat())
                    timpMediuDeAsteptare += casaDeMarcat.getTimpDeAsteptareTotalLaCasa();
                timpMediuDeAsteptare = timpMediuDeAsteptare / numarulDeClienti;
                View.timpMediuAsteptare.setText(String.format("%3.3f", timpMediuDeAsteptare));
                View.celMaiAglomeratMoment.setText("la secunda " + celMaiAglomeratMoment + " au fost " + numarulMaximDeOameniInMagazin + " oameni.");
                View.liveInfo.setText(View.liveInfo.getText() + "TIMPUL DE SIMULARE S-A SFARSIT.\n CASELE SUNT ACUM INCHISE.");
            }
        }
    }

    public static int getTimpCurent() {
        return timpCurent;
    }

    public static int getTimpSimulare() {
        return timpSimulare;
    }

    public static void setAccelerare(int accelerareData) {
        accelerare = accelerareData;
    }
}