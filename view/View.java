package view;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionListener;

public class View {
    private JFrame frame;
    private JPanel panouPrincipal;
    private JPanel panouVizualizare;
    private JPanel panelIntroducereDate;
    private JPanel panelPentruLabeluri;
    private JPanel panelPentruTextFielduri;
    private JPanel panouButoneRaport;

    private JLabel timpSimulare;
    private JLabel timpMinimDeProcesare;
    private JLabel timpMaximDeProcesare;
    private JLabel numarulDeCase;
    private JLabel numarulDeClienti;
    private JLabel speed;

    private JTextField tfTimpSimulare;
    private JTextField tfTimpMinimDeProcesare;
    private JTextField tfTimpMaximDeProcesare;
    private JTextField tfNumarulDeCase;
    private JTextField tfNumarulDeClienti;
    private JSlider speedSlider;

    private JButton playButton;
    public static TextArea caseDeMarcat;
    public static TextArea liveInfo;
    private Font font = new Font("Arial", Font.BOLD, 12);
    private Font fontMarit = new Font("Arial", Font.BOLD, 14);

    private JLabel etichetaTimpMediuAsteptare;
    public static JLabel timpMediuAsteptare;
    private JLabel etichetaCelMaiAglomeratMoment;
    public static JLabel celMaiAglomeratMoment;

    public View() {
        frame = new JFrame("Magazin");
        panouPrincipal = new JPanel();
        panouPrincipal.setLayout(new BoxLayout(panouPrincipal, BoxLayout.Y_AXIS));
        panelPentruLabeluri = new JPanel();
        panelPentruLabeluri.setLayout(new BoxLayout(panelPentruLabeluri, BoxLayout.X_AXIS));
        timpSimulare = new JLabel("Timp Simulare                             ");
        panelPentruLabeluri.add(timpSimulare);
        panelPentruLabeluri.add(Box.createHorizontalStrut(20));
        timpMinimDeProcesare = new JLabel("Timp Minim Procesare              ");
        panelPentruLabeluri.add(timpMinimDeProcesare);
        panelPentruLabeluri.add(Box.createHorizontalStrut(20));
        timpMaximDeProcesare = new JLabel("Timp Maxim Procesare              ");
        panelPentruLabeluri.add(timpMaximDeProcesare);
        panelPentruLabeluri.add(Box.createHorizontalStrut(20));
        numarulDeCase = new JLabel("Numar Case                                  ");
        panelPentruLabeluri.add(numarulDeCase);
        panelPentruLabeluri.add(Box.createHorizontalStrut(20));
        numarulDeClienti = new JLabel("Numar Clienti                               ");
        panelPentruLabeluri.add(numarulDeClienti);
        panelPentruLabeluri.add(Box.createHorizontalStrut(20));
        speed = new JLabel("Speed                                                        ");
        panelPentruLabeluri.add(speed);
        panelPentruTextFielduri = new JPanel();
        panelPentruTextFielduri.setLayout(new BoxLayout(panelPentruTextFielduri, BoxLayout.X_AXIS));
        tfTimpSimulare = new JTextField(15);
        tfTimpSimulare.setFont(font);
        panelPentruTextFielduri.add(tfTimpSimulare);
        panelPentruTextFielduri.add(Box.createHorizontalStrut(20));
        tfTimpMinimDeProcesare = new JTextField(15);
        tfTimpMinimDeProcesare.setFont(font);
        panelPentruTextFielduri.add(tfTimpMinimDeProcesare);
        panelPentruTextFielduri.add(Box.createHorizontalStrut(20));
        tfTimpMaximDeProcesare = new JTextField(15);
        tfTimpMaximDeProcesare.setFont(font);
        panelPentruTextFielduri.add(tfTimpMaximDeProcesare);
        panelPentruTextFielduri.add(Box.createHorizontalStrut(20));
        tfNumarulDeCase = new JTextField(15);
        tfNumarulDeCase.setFont(font);
        panelPentruTextFielduri.add(tfNumarulDeCase);
        panelPentruTextFielduri.add(Box.createHorizontalStrut(20));
        tfNumarulDeClienti = new JTextField(15);
        tfNumarulDeClienti.setFont(font);
        panelPentruTextFielduri.add(tfNumarulDeClienti);
        panelPentruTextFielduri.add(Box.createHorizontalStrut(20));
        speedSlider = new JSlider(0, 1, 10, 1);
        speedSlider.setPaintTicks(true);
        speedSlider.setMajorTickSpacing(1);
        speedSlider.setPaintLabels(true);
        speedSlider.setSnapToTicks(true);
        panelPentruTextFielduri.add(speedSlider);
        panouVizualizare = new JPanel();
        panouVizualizare.setLayout(new BoxLayout(panouVizualizare, BoxLayout.X_AXIS));
        caseDeMarcat = new TextArea(10, 65);
        caseDeMarcat.setFont(fontMarit);
        caseDeMarcat.setEditable(false);
        liveInfo = new TextArea(10, 10);
        liveInfo.setFont(fontMarit);
        liveInfo.setEditable(false);
        panouVizualizare.setPreferredSize(new Dimension(frame.getWidth(), 400));
        panouVizualizare.add(caseDeMarcat);
        panouVizualizare.add(liveInfo);
        panouButoneRaport = new JPanel();
        panouButoneRaport.setLayout(new BoxLayout(panouButoneRaport, BoxLayout.X_AXIS));
        panouButoneRaport.setBorder(new EmptyBorder(0, 10, 10, 10));
        playButton = new JButton(new ImageIcon(getClass().getResource("play-button.png")));
        playButton.setBackground(Color.WHITE);
        playButton.setFocusPainted(false);
        etichetaTimpMediuAsteptare = new JLabel("Timp mediu de asteptare: ");
        panouButoneRaport.add(etichetaTimpMediuAsteptare);
        timpMediuAsteptare = new JLabel("       ");
        panouButoneRaport.add(timpMediuAsteptare);
        etichetaCelMaiAglomeratMoment = new JLabel("        Cel mai aglomerat moment: ");
        panouButoneRaport.add(etichetaCelMaiAglomeratMoment);
        celMaiAglomeratMoment = new JLabel("                                                                ");
        panouButoneRaport.add(celMaiAglomeratMoment);
        panouButoneRaport.add(Box.createHorizontalStrut(560));
        panouButoneRaport.setBorder(new EmptyBorder(0, 10, 10, 10));
        panouButoneRaport.add(playButton);
        panelIntroducereDate = new JPanel();
        panelIntroducereDate.setLayout(new BoxLayout(panelIntroducereDate, BoxLayout.Y_AXIS));
        panelIntroducereDate.add(panelPentruLabeluri);
        panelIntroducereDate.add(panelPentruTextFielduri);
        panelIntroducereDate.setBorder(new EmptyBorder(10, 10, 10, 10));
        panouPrincipal.add(panouVizualizare);
        panouPrincipal.add(panelIntroducereDate);
        panouPrincipal.add(panouButoneRaport);
        frame.setContentPane(panouPrincipal);
        frame.pack();
    }

    public void addTimpSimulareListener(ActionListener timpSimularel) {
        tfTimpSimulare.addActionListener(timpSimularel);
    }

    public void addTimpMinProcListener(ActionListener timpMinProcl) {
        tfTimpMinimDeProcesare.addActionListener(timpMinProcl);
    }

    public void addTimpMaxProcListener(ActionListener timpMaxProcl) {
        tfTimpMaximDeProcesare.addActionListener(timpMaxProcl);
    }

    public void addNumarCaseListener(ActionListener numarCasel) {
        tfNumarulDeCase.addActionListener(numarCasel);
    }

    public void addNumarClientiListener(ActionListener numarClienti) {
        tfNumarulDeClienti.addActionListener(numarClienti);
    }

    public void addSpeedListener(ChangeListener speedl) {
        speedSlider.addChangeListener(speedl);
    }

    public void addPlayListener(ActionListener playl) {
        playButton.addActionListener(playl);
    }

    public JTextField getTfTimpSimulare() {
        return tfTimpSimulare;
    }

    public JTextField getTfTimpMinimDeProcesare() {
        return tfTimpMinimDeProcesare;
    }

    public JTextField getTfTimpMaximDeProcesare() {
        return tfTimpMaximDeProcesare;
    }

    public JTextField getTfNumarulDeCase() {
        return tfNumarulDeCase;
    }

    public JTextField getTfNumarulDeClienti() {
        return tfNumarulDeClienti;
    }

    public JSlider getSpeedSlider() {
        return speedSlider;
    }

    public JFrame getFrame() {
        return frame;
    }
}
